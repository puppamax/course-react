import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import './media/bootstrap/dist/css/bootstrap.css';

class App extends Component {
  render() {
    return (
      <div className="App">
          <div className="container emp-profile">
              <form method="post">
                  <div className="row">
                      <div className="col-md-4">

                          <div className="profile-img">
                              <img src="profile.jpg" alt=""/>
                          </div>
                            <br/>
                          <br/>
                          <div className="profile-work">
                              <h5>Name</h5>
                              <p>Jirapat Soisin</p>

                              <h5>About</h5>
                              <label>I'm from OOCA</label>
                                <br/>
                              <br/>
                              <br/>
                              <h5>Skills</h5>
                              <p>PHP</p>
                              <div className="progress">
                                  <div className="progress-bar w-75" role="progressbar" aria-valuenow="75"
                                       aria-valuemin="0" aria-valuemax="100"></div>
                              </div>

                              <p>Java</p>
                              <div className="progress">
                                  <div className="progress-bar w-75" role="progressbar" aria-valuenow="75"
                                       aria-valuemin="0" aria-valuemax="100"></div>
                              </div>

                              <p>C#</p>
                              <div className="progress">
                                  <div className="progress-bar w-75" role="progressbar" aria-valuenow="75"
                                       aria-valuemin="0" aria-valuemax="100"></div>
                              </div>

                          </div>
                      </div>
                      <div className="col-md-8">
                          <div className="tab-content profile-tab " id="myTabContent">

                                  <div className="row">
                                      <div className="col-md-12">
                                          <p>Experience</p>
                                      </div>
                                      <div className="col-md-12">
                                          <label> OOCA | 1 May 2019 – NOW</label><br/>
                                          <label>Position: Developer</label>
                                      </div>
                                      <div className="col-md-12">
                                          <label> SVOA | SVOA PUBLIC COMPANY LIMITED | 1 DEC 2017 – 28 FEB 2019</label><br/>
                                          <label>Position: Developer</label>
                                      </div>
                                      <div className="col-md-12">
                                          <label> KNPPLUS | KNP CO., LTD. | 10 MACH 2016 – 30 JAN 2017</label><br/>
                                          <label>Position: Developer</label>
                                      </div>
                                      <div className="col-md-12">
                                          <label> SAKURAWORKJAPAN | ARMS (THAILAND) CO., LTD. | 15 OCT 2015 – 15 JAN 2016</label><br/>
                                          <label>Position: Developer</label>
                                      </div>
                                  </div>
                                  <div className="row">
                                      <div className="col-md-12">
                                          <p>Education</p>
                                      </div>

                                      <div className="col-md-12">
                                          <label>BACHELOR DEGREE| 2014 – 2017| RANGSIT UNIVERSITY</label><br/>
                                          <label>Major: Computer Science</label>
                                      </div>
                                      <div className="col-md-12">
                                          <label>ASSOCIATE DEGREE | 2012 - 2014 | EASTERN TECHNOLOGICAL COLLEGE</label><br/>
                                          <label>Major: Information Technology</label>
                                      </div>
                                      <div className="col-md-12">
                                          <label>VOCATIONAL CERTIFICATE| 2010 - 2012 | EASTERN TECHNOLOGICAL COLLEGE</label><br/>
                                          <label>Major: Computer Business</label>
                                      </div>
                                  </div>
                          </div>
                      </div>
                  </div>

              </form>
          </div>
      </div>
    );
  }
}

export default App;
